/** \file cmd_processor.c
 * simple command processor for test console
 *
 * Copyright (c) 2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 */

#include <asf.h>
#include <string.h>
#include <stdio.h>
#include "cbuf.h"
#include "cryptoauthlib.h"
#include "test/atca_unit_tests.h"
#include "test/atca_basic_tests.h"
#include "test/atca_crypto_sw_tests.h"
#include "utility.h"
#include "aes132_helper.h"
#include "aes132_comm.h"
#include "aes132_comm_marshaling.h"
#include "aes132_spi_unified.h"

#include "cmd-processor.h"

int help(void)
{
	printf("\r\nUsage:\r\n");
	printf("508  - set target device to ATECC508A\r\n");
	printf("204  - set target device to ATSHA204A\r\n");
	printf("108  - set target device to ATECC108A\r\n");
	printf("u204 - run unit tests for SHA204A\r\n");
	printf("b204 - run basic tests for SHA204A\r\n");
	printf("u108 - run unit tests for ECC108A\r\n");
	printf("u508 - run unit tests for ECC508A\r\n");
	printf("b508 - run basic tests on ECC508A\r\n");
	printf("lockstat - zone lock status\r\n");
	printf("lockcfg  - lock config zone\r\n");
	printf("lockdata - lock data and OTP zones\r\n");
	printf("cd  - run unit tests on cert data\r\n");
	printf("cio - run unit tests on cert i/o\r\n");
	printf("info - get the chip revision\r\n");
	printf("sernum - get the chip serial number\r\n");
	printf("crypto - run unit tests for software crypto functions\r\n");
	printf("rand - generate some random numbers\r\n");
	printf("discover - buses and devices\r\n");

	printf("\r\n");
	return ATCA_SUCCESS;
}

uint8_t cmdbytes[128];


ATCA_STATUS isDeviceLocked( uint8_t zone, bool *isLocked )
{
	ATCA_STATUS status;

	atcab_init( gCfg );
	status = atcab_is_locked(zone, isLocked );
	atcab_release();
	return status;
}

ATCA_STATUS lockstatus(void)
{
	ATCA_STATUS status;
	bool dataIsLocked = 0xff, cfgIsLocked = 0xff;

	if ( (status = isDeviceLocked( LOCK_ZONE_CONFIG, &cfgIsLocked )) != ATCA_SUCCESS )
		printf("can't read cfg lock\r\n");

	if ( (status = isDeviceLocked( LOCK_ZONE_DATA, &dataIsLocked )) != ATCA_SUCCESS )
		printf("can't read data lock\r\n");

	printf("Config Zone Lock: %s\r\n", cfgIsLocked == 0x01 ? "locked" : "unlocked");
	printf("Data Zone Lock  : %s\r\n", dataIsLocked == 0x01 ? "locked" : "unlocked" );

	return status;
}

uint8_t lock_config_zone(void)
{
	ATCA_STATUS status;
	uint8_t lock_response;

	atcab_init( gCfg );

	if ( (status = atcab_lock_config_zone( &lock_response )) != ATCA_SUCCESS )
		printf("\r\nunable to lock config zone\r\n");

	atcab_release();
	return lock_response;
}

uint8_t lock_data_zone(void)
{
	ATCA_STATUS status;
	uint8_t lock_response;

	atcab_init( gCfg );
	if ( (status = atcab_lock_data_zone( &lock_response )) != ATCA_SUCCESS )
		printf("\r\nunable to lock data zone\r\n");

	atcab_release();
	return lock_response;
}

ATCA_STATUS getinfo( uint8_t *revision )
{
	ATCA_STATUS status;

	atcab_init( gCfg );
	status =  atcab_info( revision );
	atcab_release();

	return status;
}

ATCA_STATUS getsernum( uint8_t *sernum )
{
	ATCA_STATUS status;

	atcab_init( gCfg );
	status = atcab_read_serial_number( sernum );
	atcab_release();

	return status;
}

ATCA_STATUS doRandoms()
{
	ATCA_STATUS status;
	uint8_t randout[RANDOM_RSP_SIZE];
	char displayStr[ RANDOM_RSP_SIZE * 3];
	int displen = sizeof(displayStr);
	int i;

	atcab_init( gCfg );
	for ( i = 0; i < 5; i++ ) {
		if ( (status = atcab_random( randout )) != ATCA_SUCCESS )
			break;

		atcab_bin2hex(randout, 32, displayStr, &displen);
		printf("%s\r\n", displayStr);
	}

	if ( status != ATCA_SUCCESS )
		printf("error: %02x\r\n", status );

	atcab_release();

	return status;
}

int parseCmd( char *commands )
{
	char *cmds = NULL;
	ATCA_STATUS status;
	ATCAIfaceCfg ifaceCfgs[10];
	char *devname[] = { "ATSHA204A", "ATECC108A", "ATECC508A" };  // indexed by ATCADeviceType
	int i;

	if ( (cmds = strstr( commands, "help")) )
		help();
	else if ( (cmds = strstr( commands, "u508")) )
		atca_unit_tests(ATECC508A);
	else if ( (cmds = strstr( commands, "b508")) )
		atca_basic_tests(ATECC508A);
	else if ( (cmds = strstr( commands, "u108")) )
		atca_unit_tests(ATECC108A);
	else if ( (cmds = strstr( commands, "b108")) )
		atca_basic_tests(ATECC108A);
	else if ( (cmds = strstr( commands, "u204")) )
		atca_unit_tests(ATSHA204A);
	else if ( (cmds = strstr( commands, "b204")) )
		atca_basic_tests(ATSHA204A);
	else if ( (cmds = strstr( commands, "cd")) )
		certdata_unit_tests();
	else if ( (cmds = strstr( commands, "cio")) )
		certio_unit_tests();
	else if ( (cmds = strstr( commands, "lockstat")) ) {
		if ( gCfg == NULL ) {
			printf("setting default target\r\n");
			gCfg = &cfg_sha204a_i2c_default;
		}
		lockstatus();
	} else if ( (cmds = strstr( commands, "lockcfg")) ) {
		if ( lock_config_zone() != ATCA_SUCCESS )
			printf("Could not lock config zone\r\n");
		lockstatus();
	} else if ( (cmds = strstr( commands, "lockdata")) ) {
		if ( lock_data_zone() != ATCA_SUCCESS )
			printf("Could not lock data zone\r\n");
		lockstatus();
	} else if ( (cmds = strstr( commands, "508") ) ) {
		gCfg = &cfg_ateccx08a_i2c_default;
		gCfg->devtype = ATECC508A;
		printf("\r\ncurrent device: ATECC508A\r\n");
	} else if ( (cmds = strstr( commands, "204") ) ) {
		gCfg = &cfg_sha204a_i2c_default;
		printf("\r\ncurrent device: ATSHA204A\r\n");
	} else if ( (cmds = strstr( commands, "108") ) ) {
		gCfg = &cfg_ateccx08a_i2c_default;
		gCfg->devtype = ATECC108A;
		printf("\r\ncurrent device: ATECC108A\r\n");
	} else if ( (cmds = strstr( commands, "info")) ) {
		uint8_t revision[4];
		char displaystr[15];
		int displaylen = sizeof(displaystr);

		gCfg = &cfg_ateccx08a_i2c_default;
		status = getinfo(revision);
		if ( status == ATCA_SUCCESS ) {
			// dump revision
			atcab_bin2hex(revision, 4, displaystr, &displaylen );
			printf("\r\nx08 revision:\r\n%s\r\n", displaystr);
		}

		gCfg = &cfg_sha204a_i2c_default;
		status = getinfo(revision);
		if ( status == ATCA_SUCCESS ) {
			// dump revision
			atcab_bin2hex(revision, 4, displaystr, &displaylen );
			printf("\r\nsha204 revision:\r\n%s\r\n", displaystr);
		}

	} else if ( (cmds = strstr( commands, "sernum")) ) {
		uint8_t serialnum[ATCA_SERIAL_NUM_SIZE];
		char displaystr[30];
		int displaylen = sizeof(displaystr);

		gCfg = &cfg_ateccx08a_i2c_default;
		status = getsernum(serialnum);
		if ( status == ATCA_SUCCESS ) {
			// dump serial num
			atcab_bin2hex(serialnum, ATCA_SERIAL_NUM_SIZE, displaystr, &displaylen );
			printf("\r\nx08 serial number:\r\n%s\r\n", displaystr);
		}

		gCfg = &cfg_sha204a_i2c_default;
		status = getsernum(serialnum);
		if ( status == ATCA_SUCCESS ) {
			// dump serial num
			atcab_bin2hex(serialnum, ATCA_SERIAL_NUM_SIZE, displaystr, &displaylen );
			printf("\r\n204 serial number:\r\n%s\r\n", displaystr);
		}
	} else if ( (cmds = strstr( commands, "crypto")) )
		atca_crypto_sw_tests();
	else if ( (cmds = strstr( commands, "rand")) ) {
		if ( gCfg == NULL ) {
			printf("setting default target\r\n");
			gCfg = &cfg_ateccx08a_i2c_default;
			gCfg->devtype = ATECC508A;
		}
		doRandoms();
	} else if ( (cmds = strstr( commands, "discover")) ) {


		for ( i = 0; i < sizeof( ifaceCfgs ) / sizeof(ATCAIfaceCfg); i++ ) {
			ifaceCfgs[i].devtype = ATCA_DEV_UNKNOWN;
			ifaceCfgs[i].iface_type = ATCA_DEV_UNKNOWN;
		}

		printf("\r\n...looking\r\n");
		atcab_cfg_discover( ifaceCfgs, sizeof(ifaceCfgs) / sizeof(ATCAIfaceCfg) );

		for ( i = 0; i < sizeof( ifaceCfgs) / sizeof(ATCAIfaceCfg); i++ )
			if ( ifaceCfgs[i].devtype != ATCA_DEV_UNKNOWN && ifaceCfgs[i].iface_type == ATCA_I2C_IFACE )
				printf("Found %s @ bus %d addr %02x\r\n", devname[ifaceCfgs[i].devtype], ifaceCfgs[i].atcai2c.bus, ifaceCfgs[i].atcai2c.slave_address );
	
	}else if ( (cmds = strstr( commands, "showconf")) ) {
		char displaystr[ATCA_SHA_CONFIG_SIZE*4];
		int displaylen = sizeof(displaystr);
		unsigned char config_data[ATCA_SHA_CONFIG_SIZE];
		gCfg = &cfg_sha204a_i2c_default;
		atcab_init( gCfg );
		atcab_read_sha_config_zone(config_data);
		if ( status == ATCA_SUCCESS ) {
			// dump serial num
			atcab_bin2hex(config_data, ATCA_SHA_CONFIG_SIZE, displaystr, &displaylen );
			printf("\r\n204 config zone:\r\n%s\r\n", displaystr);
		}
	}else if ( (cmds = strstr( commands, "writeSN")) ) {
		char displaystr[32];
		int displaylen = sizeof(displaystr);
		unsigned char config_data[12] = {0x01, 0x23, 0x8C, 0x69,
										 0x0A, 0xFD, 0x38, 0xB9,
										 0xEE, 0x16, 0xFE, 0x00};
		gCfg = &cfg_sha204a_i2c_default;
		atcab_init( gCfg );
		atcab_write_bytes_zone(ATSHA204A, ATCA_ZONE_CONFIG, 0x00, &config_data[0], 4);
		if ( status != ATCA_SUCCESS ) {
			printf("Write SN[0:3] FAIL\r\n");
		}
		atcab_write_bytes_zone(ATSHA204A, ATCA_ZONE_CONFIG, 0x08, &config_data[4], 4);
		if ( status != ATCA_SUCCESS ) {
			printf("Write SN[4:7] FAIL\r\n");
		}
		atcab_write_bytes_zone(ATSHA204A, ATCA_ZONE_CONFIG, 0x0B, &config_data[8], 4);
		if ( status != ATCA_SUCCESS ) {
			printf("Write SN[8] FAIL\r\n");
		}
		atcab_release();
	}
	
	else if ((cmds = strstr( commands, "check")))
	{
		
				struct i2c_master_module i2c_master_discover;
				struct i2c_master_config config_i2c_master_discover;
				
				uint8_t ret_code;
				uint8_t data[10],expected[4] = { 0x04, 0x11, 0x33, 0x43 };
				uint8_t check[] = {0x03, 0x07,0x30,0x00,0x00,0x00,0x03,0x5D};
				uint8_t check_aes132[] = {0xFE,0x00,0x09,0x0C, 0x00, 0x00, 0x06, 0x00, 0x00,0xA9, 0xE7};
				uint8_t *txdata = check;
				uint8_t *txdataaes = check_aes132;
				uint8_t revision[4];
				uint32_t bdrt = 400000;
				int retries = 20;
				int status = !STATUS_OK;
				char displaystr[15];
				int displaylen = sizeof(displaystr);
				
				uint8_t g_tx_buffer[AES132_COMMAND_SIZE_MAX];
				uint8_t g_rx_buffer[AES132_RESPONSE_SIZE_MAX];
				//Check interface
				
				
				i2c_master_get_config_defaults(&config_i2c_master_discover);
				config_i2c_master_discover.baud_rate = 400;
				
				i2c_master_init( &i2c_master_discover, SERCOM2, &config_i2c_master_discover);
				
				i2c_master_enable(&i2c_master_discover);
				
				//implement wake up pulse 
				
				//packet to receive data
				struct i2c_master_packet packet = {
					.address			= 0x00,
					.data_length		= 0,
					.data				= &data[0],
					.ten_bit_address	= false,
					.high_speed			= false,
					.hs_master_code		= 0x0,
				};
				
				//packet to be sent
				struct i2c_master_packet packet1 = {
					.address			= 0x00,
					.data_length		= 0,
					.data				= &data[0],
					.ten_bit_address	= false,
					.high_speed			= false,
					.hs_master_code		= 0x0,
				};
				
				for (packet.address=0x00;packet.address<=0xD0;packet.address++)
				{
					config_i2c_master_discover.buffer_timeout = 10000;
					config_i2c_master_discover.baud_rate = 100;

					i2c_master_disable(&i2c_master_discover);

					i2c_master_init(&i2c_master_discover, SERCOM2, &config_i2c_master_discover);
					
					i2c_master_enable(&i2c_master_discover);
		
					i2c_master_write_packet_wait(  &i2c_master_discover, &packet1 );
					
					atca_delay_us(2560);
					
					
					packet.data_length = 4;
					packet.data = data;
					//while ( retries-- > 0 && status != STATUS_OK )
					i2c_master_read_packet_wait( &i2c_master_discover, &packet);
					
						if ( bdrt != 100000 )
						{
							config_i2c_master_discover.buffer_timeout = 10000;
							config_i2c_master_discover.baud_rate = 400;

							i2c_master_disable(&i2c_master_discover);


							i2c_master_init(  &i2c_master_discover, SERCOM2, &config_i2c_master_discover);
						
							i2c_master_enable(&i2c_master_discover);
						}

						if ( memcmp( data, expected, 4 ) == 0 )
						{
							printf("\r\n device I2C Address 0x%.2X\r\n",packet.address<<1);
							memset(data, 0x00, sizeof(data));
						
							packet1.address = packet.address;
							packet1.data_length = 8;
							packet1.data =txdata;
						
							i2c_master_write_packet_wait(  &i2c_master_discover, &packet1 );
						
							atca_delay_ms(2);
						
							packet.data_length = 7;
							packet.data = data;
							//while ( retries-- > 0 && status != STATUS_OK )
							i2c_master_read_packet_wait( &i2c_master_discover, &packet);
						
						if (memcmp(data[3],0x50,1)==0)
						{
							printf("\r\n Found ECC508 with device address 0x%.2X\r\n",packet.address<<1);
							//break;
						}
						
						else if(memcmp(data[3],0x00,1)==0)
						{
							printf("\r\n Found SHA204 with device address 0x%.2X\r\n",packet.address<<1);
							//break;
						}
						

					}
				}
				
				//Check AES132A I2C Interface
				for (packet.address=0x00;packet.address<=0xC0;packet.address++)
				{
					
					packet1.address = packet.address;
					packet1.data_length =11;
					packet1.data =txdataaes;
					
					i2c_master_write_packet_wait(  &i2c_master_discover, &packet1 );
					
					atca_delay_ms(2);
					
					packet.data_length = 6;
					packet.data = data;
					//while ( retries-- > 0 && status != STATUS_OK )
					i2c_master_read_packet_wait( &i2c_master_discover, &packet);
					
					if (memcmp(data[2],0x0A,1)==0)
					{
						printf("\r\n Found AES132A with device address 0x%.2X\r\n",packet.address<<1);
						memset(data, 0xFF, sizeof(data));
						//break;
					}

				}
		
				/*
				gCfg = &cfg_ateccx08a_i2c_default;
				status = getinfo(revision);
				if ( status == ATCA_SUCCESS ) {
					// dump revision
					atcab_bin2hex(revision, 4, displaystr, &displaylen );
					if(revision[2]== 80 )
					{
						printf("\r\nDevice ECC508 I2C interface");
					}
					status = ATCA_SUCCESS;
				}

				gCfg = &cfg_sha204a_i2c_default;
				status = getinfo(revision);
				if ( status == ATCA_SUCCESS ) {
					// dump revision
					atcab_bin2hex(revision, 4, displaystr, &displaylen );
					if(revision[2]== 00)
					{
						printf("\r\nDevice SHA204 I2C interface");
					}
					status = ATCA_SUCCESS;
				}
				
						#ifdef ATCA_HAL_I2C
						#undef ATCA_HAL_I2C
						
						gCfg = &cfg_ateccx08a_swi_default;
						status = getinfo(revision);
						if ( status == ATCA_SUCCESS ) {
							// dump revision
							atcab_bin2hex(revision, 4, displaystr, &displaylen );
							if(revision[2]== 80 )
							{
								printf("\r\nDevice ECC508 SWI interface");
								printf("\r\nsha204 revision:\r\n%d %d %d %d\r\n", revision[0],revision[1],revision[2],revision[3]);
							}
							status = ATCA_SUCCESS;
						}

						
						
						gCfg = &cfg_sha204a_swi_default;
						status = getinfo(revision);
						if ( status == ATCA_SUCCESS ) {
							// dump revision
							atcab_bin2hex(revision, 4, displaystr, &displaylen );
							if(revision[2]== 00)
							{
								printf("\r\nDevice SHA204 SWI interface");
								printf("\r\nsha204 revision:\r\n%d %d %d %d\r\n", revision[0],revision[1],revision[2],revision[3]);
							}
							
						}
						
						#define ATCA_HAL_I2C
						#endif
						
			aes132p_enable_interface();
			
			ret_code = aes132m_execute(AES132_OPCODE_INFO, 0x00, 0x0006, 0x0000,0,NULL,0,NULL,0,NULL,0,NULL,g_tx_buffer,g_rx_buffer);
			
			if(ret_code == 0)
			printf("\r\nDevice AES132 I2C interface");
			
			aes132p_disable_interface();
			
			spi_aes132p_enable_interface();
			
			ret_code = aes132m_execute(AES132_OPCODE_INFO, 0x00, 0x0006, 0x0000,0,NULL,0,NULL,0,NULL,0,NULL,g_tx_buffer,g_rx_buffer);
			
			if(ret_code == 0)
			printf("\r\nDevice AES132 SPI interface");
			
			
			spi_aes132p_enable_interface();
			*/
	}
	
	else if ((cmds = strstr( commands, "checkswi")))
	{
		uint8_t ret_code;
		uint8_t revision[4];
		char displaystr[15];
		int displaylen = sizeof(displaystr);
		
		//Check interface
		
		
		
		#ifdef ATCA_HAL_I2C
		#undef ATCA_HAL_I2C
		
		gCfg = &cfg_ateccx08a_swi_default;
		status = getinfo(revision);
		if ( status == ATCA_SUCCESS ) {
			// dump revision
			atcab_bin2hex(revision, 4, displaystr, &displaylen );
			if(revision[2]== 80 )
			{
				printf("\r\nDevice ECC508 SWI interface");
				printf("\r\nsha204 revision:\r\n%d %d %d %d\r\n", revision[0],revision[1],revision[2],revision[3]);
			}
			
		}

		
		
		gCfg = &cfg_sha204a_swi_default;
		status = getinfo(revision);
		if ( status == ATCA_SUCCESS ) {
			// dump revision
			atcab_bin2hex(revision, 4, displaystr, &displaylen );
			if(revision[2]== 00)
			{
				printf("\r\nDevice SHA204 SWI interface");
				printf("\r\nsha204 revision:\r\n%d %d %d %d\r\n", revision[0],revision[1],revision[2],revision[3]);
			}
			
		}
		
		#define ATCA_HAL_I2C
		#endif
	}
	
	 else if ( strlen(commands) )
		printf("\r\nsyntax error in command: %s\r\n", commands);

	return ATCA_SUCCESS;
}


int processCmd(void)
{
	static char cmd[256];
	uint16_t i = 0;

	while ( !CBUF_IsEmpty(cmdQ) && i < sizeof(cmd))
		cmd[i++] = CBUF_Pop( cmdQ );
	cmd[i] = '\0';
	//printf("\r\n%s\r\n", command );
	parseCmd(cmd);
	printf("$ ");

	return ATCA_SUCCESS;
}
