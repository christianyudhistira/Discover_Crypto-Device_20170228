var searchData=
[
  ['software_20crypto_20methods_20_28atcac_5f_29',['Software crypto methods (atcac_)',['../a00177.html',1,'']]],
  ['serverpubkey',['serverPubKey',['../a00105.html#a84cc6ccc8738092d7a5ce66ed7b482a8',1,'atcatls_tests.c']]],
  ['setup',['setUp',['../a00164.html#a95c834d6178047ce9e1bce7cbfea2836',1,'setUp(void):&#160;unity_fixture.c'],['../a00166.html#a95c834d6178047ce9e1bce7cbfea2836',1,'setUp(void):&#160;unity_fixture.c']]],
  ['sha1_5froutines_2ec',['sha1_routines.c',['../a00138.html',1,'']]],
  ['sha1_5froutines_2eh',['sha1_routines.h',['../a00139.html',1,'']]],
  ['sha204_5fdefault_5fconfig',['sha204_default_config',['../a00057.html#a561f6a9ae0159bf6a23c0bbc65a4a363',1,'atca_basic_tests.c']]],
  ['sha256_5fblock_5fsize',['SHA256_BLOCK_SIZE',['../a00141.html#a9c1fe69ad43d4ca74b84303a0ed64f2f',1,'SHA256_BLOCK_SIZE():&#160;sha2_routines.h'],['../a00082.html#a9c1fe69ad43d4ca74b84303a0ed64f2f',1,'SHA256_BLOCK_SIZE():&#160;atca_host.c']]],
  ['sha256_5fdigest_5fsize',['SHA256_DIGEST_SIZE',['../a00141.html#a81efbc0fc101b06a914f7ff9e2fbc0e9',1,'sha2_routines.h']]],
  ['sha2_5froutines_2ec',['sha2_routines.c',['../a00140.html',1,'']]],
  ['sha2_5froutines_2eh',['sha2_routines.h',['../a00141.html',1,'']]],
  ['sha_5fcount_5flong',['SHA_COUNT_LONG',['../a00062.html#ada37181a648dd51b374eec1cbb4bbaa3',1,'atca_command.h']]],
  ['sha_5fcount_5fshort',['SHA_COUNT_SHORT',['../a00062.html#a49e390c29b6fc05df227604c169b86b0',1,'atca_command.h']]],
  ['sha_5fdata_5fmax',['SHA_DATA_MAX',['../a00062.html#ad89ca95de067d12cd2f35a60b0e997b1',1,'atca_command.h']]],
  ['sha_5fhmac_5fend_5fmask',['SHA_HMAC_END_MASK',['../a00062.html#a0963088510e08e9b5c59adbbcb748e63',1,'atca_command.h']]],
  ['sha_5fhmac_5fstart_5fmask',['SHA_HMAC_START_MASK',['../a00062.html#a77f09d03f9bd8cd246ee2a90e74da8e2',1,'atca_command.h']]],
  ['sha_5fmode_5fhmac_5fend',['SHA_MODE_HMAC_END',['../a00062.html#a46d1dac1252232be5fc7f3f0e90959dba526d05786f9fafb5e3adff53cdabf1b6',1,'atca_command.h']]],
  ['sha_5fmode_5fhmac_5fstart',['SHA_MODE_HMAC_START',['../a00062.html#a46d1dac1252232be5fc7f3f0e90959dba42b05919c478458ba70dd5ad74d9e61e',1,'atca_command.h']]],
  ['sha_5fmode_5fhmac_5fupdate',['SHA_MODE_HMAC_UPDATE',['../a00062.html#a46d1dac1252232be5fc7f3f0e90959dba33ac16163deda30913906a5e0083035d',1,'atca_command.h']]],
  ['sha_5fmode_5fsha256_5fend',['SHA_MODE_SHA256_END',['../a00062.html#a46d1dac1252232be5fc7f3f0e90959dba2ba712179cfc98b649727cf50115a261',1,'atca_command.h']]],
  ['sha_5fmode_5fsha256_5fpublic',['SHA_MODE_SHA256_PUBLIC',['../a00062.html#a46d1dac1252232be5fc7f3f0e90959dbab3cde4fde2d2b6b02c9a09c15eebde4e',1,'atca_command.h']]],
  ['sha_5fmode_5fsha256_5fstart',['SHA_MODE_SHA256_START',['../a00062.html#a46d1dac1252232be5fc7f3f0e90959dbacd837a70defff7b7e60f625b787feeaa',1,'atca_command.h']]],
  ['sha_5fmode_5fsha256_5fupdate',['SHA_MODE_SHA256_UPDATE',['../a00062.html#a46d1dac1252232be5fc7f3f0e90959dba3fe70adf7db52be2c42ecba8d0defc51',1,'atca_command.h']]],
  ['sha_5frsp_5fsize',['SHA_RSP_SIZE',['../a00062.html#a8a5a586bdb0194be827417658836bf9c',1,'atca_command.h']]],
  ['sha_5frsp_5fsize_5flong',['SHA_RSP_SIZE_LONG',['../a00062.html#ae5ad3c81d0b62f4b86a4a0dc84a79134',1,'atca_command.h']]],
  ['sha_5frsp_5fsize_5fshort',['SHA_RSP_SIZE_SHORT',['../a00062.html#a74490b179b34f9925e9fe00675e53713',1,'atca_command.h']]],
  ['sha_5fsha256_5fend_5fmask',['SHA_SHA256_END_MASK',['../a00062.html#a11f88e62bdda2b5e4f87bf5eb3c066fa',1,'atca_command.h']]],
  ['sha_5fsha256_5fpublic_5fmask',['SHA_SHA256_PUBLIC_MASK',['../a00062.html#a66b01f0731ce8b2ccd1d9e96e77f5067',1,'atca_command.h']]],
  ['sha_5fsha256_5fstart_5fmask',['SHA_SHA256_START_MASK',['../a00062.html#a5f4dfd5ad29d90445803469877e726e2',1,'atca_command.h']]],
  ['sha_5fsha256_5fupdate_5fmask',['SHA_SHA256_UPDATE_MASK',['../a00062.html#a16a919e594c3997db4d385d3dc28d372',1,'atca_command.h']]],
  ['shaengine',['shaEngine',['../a00138.html#ace163fad49a8d5b5082c8288817de00b',1,'shaEngine(U32 *buf, U8 *_h):&#160;sha1_routines.c'],['../a00139.html#ae90c10fb85e4b2f369fa87a2e6cd54a7',1,'shaEngine(U32 *buf, U8 *h):&#160;sha1_routines.c']]],
  ['shakey_5fslot',['SHAKEY_SLOT',['../a00180.html#gac2414e5c5c512ab1bb9bbcb869f031bd',1,'atcatls_cfg.h']]],
  ['sig1',['sig1',['../a00105.html#aa5be66dae3c5fe0d237e3193e6a8db11',1,'atcatls_tests.c']]],
  ['sign_5fcount',['SIGN_COUNT',['../a00062.html#aabff3f5b7f5391c27a0329ff0c997264',1,'atca_command.h']]],
  ['sign_5fkeyid_5fidx',['SIGN_KEYID_IDX',['../a00062.html#a02f20fbee84fe680d94b94a2b2828040',1,'atca_command.h']]],
  ['sign_5fmode_5fexternal',['SIGN_MODE_EXTERNAL',['../a00062.html#a9b6844bb107f02832a6d827b8c5b0fda',1,'atca_command.h']]],
  ['sign_5fmode_5fidx',['SIGN_MODE_IDX',['../a00062.html#ae7cfb9eb789137f5ea9195a7a4f6b11e',1,'atca_command.h']]],
  ['sign_5fmode_5finclude_5fsn',['SIGN_MODE_INCLUDE_SN',['../a00062.html#a71b7f8f45dbbe8c19c0e5c6c41fcf116',1,'atca_command.h']]],
  ['sign_5fmode_5finternal',['SIGN_MODE_INTERNAL',['../a00062.html#aced5221c0f15440eb52fa9f460956443',1,'atca_command.h']]],
  ['sign_5fmode_5fmask',['SIGN_MODE_MASK',['../a00062.html#a88cc1851cedb6f2a73df4618dbc0b165',1,'atca_command.h']]],
  ['sign_5frsp_5fsize',['SIGN_RSP_SIZE',['../a00062.html#a66dba5e06f73c5df37c9d18409185f4d',1,'atca_command.h']]],
  ['signature',['signature',['../a00030.html#a7b59aadbf106da91b0777b39b78d5aaa',1,'atca_verify_in_out']]],
  ['signature_5fsize',['SIGNATURE_SIZE',['../a00180.html#ga4ccb2bce0e19deec8833fcb1d367e30d',1,'atcatls.h']]],
  ['signer_5fcert_5fslot',['SIGNER_CERT_SLOT',['../a00180.html#ga6bffbe18e205ddcb813edf51654eaf6d',1,'atcatls_cfg.h']]],
  ['signer_5fpubkey_5fslot',['SIGNER_PUBKEY_SLOT',['../a00180.html#gad3967932a450f60327a437b830bff822',1,'atcatls_cfg.h']]],
  ['size',['size',['../a00050.html#a854352f53b148adc24983a58a1866d66',1,'GuardBytes']]],
  ['slave_5faddress',['slave_address',['../a00045.html#ac93868aeda435dfb685a0a0020e5c7a4',1,'ATCAIfaceCfg']]],
  ['slot',['slot',['../a00041.html#ad23984515efd99983fa4baf3754082a1',1,'atcacert_device_loc_s']]],
  ['sn',['sn',['../a00179.html#ga7276bd20c5842e712da638d25ceb6d92',1,'atca_include_data_in_out::sn()'],['../a00179.html#ga7276bd20c5842e712da638d25ceb6d92',1,'atca_mac_in_out::sn()'],['../a00179.html#ga7276bd20c5842e712da638d25ceb6d92',1,'atca_hmac_in_out::sn()']]],
  ['sn_5fsource',['sn_source',['../a00038.html#af44c31e823af19e6efe6b418a13fafe3',1,'atcacert_def_s']]],
  ['snsrc_5fdevice_5fsn',['SNSRC_DEVICE_SN',['../a00175.html#gga813047a656af7fe578d28fd54c840e8ea338258d51f0eb1c5d7ef9f0e639f5e41',1,'atcacert_def.h']]],
  ['snsrc_5fdevice_5fsn_5fhash',['SNSRC_DEVICE_SN_HASH',['../a00175.html#gga813047a656af7fe578d28fd54c840e8eaffba4ece10bc933c6b408a2b4e234ab2',1,'atcacert_def.h']]],
  ['snsrc_5fdevice_5fsn_5fhash_5fpos',['SNSRC_DEVICE_SN_HASH_POS',['../a00175.html#gga813047a656af7fe578d28fd54c840e8eab87e9124d93ca45243eacac5b96452b7',1,'atcacert_def.h']]],
  ['snsrc_5fdevice_5fsn_5fhash_5fraw',['SNSRC_DEVICE_SN_HASH_RAW',['../a00175.html#gga813047a656af7fe578d28fd54c840e8ea2d608b2eeb6da309c1e9b2af7060d909',1,'atcacert_def.h']]],
  ['snsrc_5fpub_5fkey_5fhash',['SNSRC_PUB_KEY_HASH',['../a00175.html#gga813047a656af7fe578d28fd54c840e8ea5dadea2da6020b492727eb1da6c4bb1e',1,'atcacert_def.h']]],
  ['snsrc_5fpub_5fkey_5fhash_5fpos',['SNSRC_PUB_KEY_HASH_POS',['../a00175.html#gga813047a656af7fe578d28fd54c840e8eacaf00502661bc7b9c260ca68831928b5',1,'atcacert_def.h']]],
  ['snsrc_5fpub_5fkey_5fhash_5fraw',['SNSRC_PUB_KEY_HASH_RAW',['../a00175.html#gga813047a656af7fe578d28fd54c840e8ea83bf725c47007c842da4a8e93b1bf972',1,'atcacert_def.h']]],
  ['snsrc_5fsigner_5fid',['SNSRC_SIGNER_ID',['../a00175.html#gga813047a656af7fe578d28fd54c840e8eabda5814e7da6c10dc243749dea79ffff',1,'atcacert_def.h']]],
  ['snsrc_5fstored',['SNSRC_STORED',['../a00175.html#gga813047a656af7fe578d28fd54c840e8ea1b3e4833214ba25ba3d665135b2b6cd1',1,'atcacert_def.h']]],
  ['source_5fflag',['source_flag',['../a00179.html#gad5572057bebb984824a490803a4da129',1,'atca_temp_key']]],
  ['speed',['speed',['../a00178.html#ga218b4f7c6cc2681a99c23a3b089d68b1',1,'hal_linux_kit_cdc.c']]],
  ['std_5fcert_5felements',['std_cert_elements',['../a00038.html#a7e2ecd6c0b4cef9637e3b281d29200ce',1,'atcacert_def_s']]],
  ['stdcert_5fauth_5fkey_5fid',['STDCERT_AUTH_KEY_ID',['../a00175.html#gga77184d0c71198b489ea9b57d07da824ea6a9cddad9f6b552f0fe5f63fc57cf106',1,'atcacert_def.h']]],
  ['stdcert_5fcert_5fsn',['STDCERT_CERT_SN',['../a00175.html#gga77184d0c71198b489ea9b57d07da824ea73b3d53d4e70a48fd3fdeb0143493efc',1,'atcacert_def.h']]],
  ['stdcert_5fexpire_5fdate',['STDCERT_EXPIRE_DATE',['../a00175.html#gga77184d0c71198b489ea9b57d07da824ea886b91f34d6839fe9e9217490530c604',1,'atcacert_def.h']]],
  ['stdcert_5fissue_5fdate',['STDCERT_ISSUE_DATE',['../a00175.html#gga77184d0c71198b489ea9b57d07da824ea2f3039692a71546e581fcaf8a8a53f15',1,'atcacert_def.h']]],
  ['stdcert_5fnum_5felements',['STDCERT_NUM_ELEMENTS',['../a00175.html#gga77184d0c71198b489ea9b57d07da824ead9a44768825ebcc67750d8e4172dc60f',1,'atcacert_def.h']]],
  ['stdcert_5fpublic_5fkey',['STDCERT_PUBLIC_KEY',['../a00175.html#gga77184d0c71198b489ea9b57d07da824eaf78a51bddeca05e1a525f26792b6de68',1,'atcacert_def.h']]],
  ['stdcert_5fsignature',['STDCERT_SIGNATURE',['../a00175.html#gga77184d0c71198b489ea9b57d07da824ea8eab5679d330212bb87b2413ea82b7cc',1,'atcacert_def.h']]],
  ['stdcert_5fsigner_5fid',['STDCERT_SIGNER_ID',['../a00175.html#gga77184d0c71198b489ea9b57d07da824eadfaaad6a933423f63fc3233bd84b90d3',1,'atcacert_def.h']]],
  ['stdcert_5fsubj_5fkey_5fid',['STDCERT_SUBJ_KEY_ID',['../a00175.html#gga77184d0c71198b489ea9b57d07da824ea97400647cc539ce999c0b9f95d736585',1,'atcacert_def.h']]],
  ['stopbits',['stopbits',['../a00045.html#a9de35842403baab5738ea16162012d4f',1,'ATCAIfaceCfg']]],
  ['stored_5fvalue',['stored_value',['../a00179.html#ga42913e79e96a895282f2168e8f0c7ead',1,'atca_gen_dig_in_out']]],
  ['strcmp_5fequal',['STRCMP_EQUAL',['../a00167.html#a4e6c8a46dbdd9b13ab23f1c833e144fc',1,'unity_fixture.h']]],
  ['strcpy_5fp',['strcpy_P',['../a00139.html#a3541bc4d0b928b2faa9ca63a100d1b75',1,'sha1_routines.h']]],
  ['sw_5fsha256',['sw_sha256',['../a00140.html#a5f6c75bec312e0d2faa7d645b62c3898',1,'sw_sha256(const uint8_t *message, unsigned int len, uint8_t digest[SHA256_DIGEST_SIZE]):&#160;sha2_routines.c'],['../a00141.html#a5f6c75bec312e0d2faa7d645b62c3898',1,'sw_sha256(const uint8_t *message, unsigned int len, uint8_t digest[SHA256_DIGEST_SIZE]):&#160;sha2_routines.c']]],
  ['sw_5fsha256_5fctx',['sw_sha256_ctx',['../a00053.html',1,'']]],
  ['sw_5fsha256_5ffinal',['sw_sha256_final',['../a00140.html#ace8eb02759b23942f866ddcd2057390a',1,'sw_sha256_final(sw_sha256_ctx *ctx, uint8_t digest[SHA256_DIGEST_SIZE]):&#160;sha2_routines.c'],['../a00141.html#ace8eb02759b23942f866ddcd2057390a',1,'sw_sha256_final(sw_sha256_ctx *ctx, uint8_t digest[SHA256_DIGEST_SIZE]):&#160;sha2_routines.c']]],
  ['sw_5fsha256_5finit',['sw_sha256_init',['../a00140.html#acfb7028e6f10d29c548cbecdbfa53ac8',1,'sw_sha256_init(sw_sha256_ctx *ctx):&#160;sha2_routines.c'],['../a00141.html#acfb7028e6f10d29c548cbecdbfa53ac8',1,'sw_sha256_init(sw_sha256_ctx *ctx):&#160;sha2_routines.c']]],
  ['sw_5fsha256_5fupdate',['sw_sha256_update',['../a00140.html#a4d0c952ebe691b3337205829dc352e6e',1,'sw_sha256_update(sw_sha256_ctx *ctx, const uint8_t *msg, uint32_t msg_size):&#160;sha2_routines.c'],['../a00141.html#a7f5d8ad85631d77bfdb10dfb728bfbce',1,'sw_sha256_update(sw_sha256_ctx *ctx, const uint8_t *message, uint32_t len):&#160;sha2_routines.c']]],
  ['swi_5fbus_5fref_5fct',['swi_bus_ref_ct',['../a00178.html#ga4656caa5bd357cc71fe66f4522d89e3f',1,'hal_swi_uart.c']]],
  ['swi_5fflag_5fcmd',['SWI_FLAG_CMD',['../a00178.html#ga13c01ac16bb14fde75e9c00dcd1cc761',1,'hal_swi_uart.h']]],
  ['swi_5fflag_5fidle',['SWI_FLAG_IDLE',['../a00178.html#ga6219d68ef915fdcd734f51960ba08fb6',1,'hal_swi_uart.h']]],
  ['swi_5fflag_5fsleep',['SWI_FLAG_SLEEP',['../a00178.html#ga13ef2322176ad9b7ba3d2e23b277cce6',1,'hal_swi_uart.h']]],
  ['swi_5fflag_5ftx',['SWI_FLAG_TX',['../a00178.html#ga20af8352e13a7357650ba1cbf41349ea',1,'hal_swi_uart.h']]],
  ['swi_5fhal_5fdata',['swi_hal_data',['../a00178.html#ga95ca8e0b044cf56e85a8c581a4c703da',1,'hal_swi_uart.c']]],
  ['swi_5fuart_5fdeinit',['swi_uart_deinit',['../a00178.html#ga0b1aad3936bf003686d8db7fa2be5132',1,'swi_uart_samd21_asf.c']]],
  ['swi_5fuart_5finit',['swi_uart_init',['../a00178.html#ga003b63648f1a5f9eba274760559688f2',1,'swi_uart_samd21_asf.c']]],
  ['swi_5fuart_5fmode',['swi_uart_mode',['../a00178.html#gacc2a96c86ce4c07249b81bc6a7eb2e41',1,'swi_uart_samd21_asf.c']]],
  ['swi_5fuart_5freceive_5fbyte',['swi_uart_receive_byte',['../a00178.html#ga93f3dab17c14f71641ca970998ede823',1,'swi_uart_samd21_asf.c']]],
  ['swi_5fuart_5fsamd21_5fasf_2ec',['swi_uart_samd21_asf.c',['../a00142.html',1,'']]],
  ['swi_5fuart_5fsamd21_5fasf_2eh',['swi_uart_samd21_asf.h',['../a00143.html',1,'']]],
  ['swi_5fuart_5fsamd21_5fstart_2ec',['swi_uart_samd21_start.c',['../a00144.html',1,'']]],
  ['swi_5fuart_5fsamd21_5fstart_2eh',['swi_uart_samd21_start.h',['../a00145.html',1,'']]],
  ['swi_5fuart_5fsend_5fbyte',['swi_uart_send_byte',['../a00178.html#ga4a4d491006d9683d67838963706ac9c1',1,'swi_uart_samd21_asf.c']]],
  ['swi_5fuart_5fsetbaud',['swi_uart_setbaud',['../a00178.html#gaba296f70baf68b768d6c4bb9cf7ba67d',1,'swi_uart_samd21_asf.c']]],
  ['swi_5fwake_5ftoken',['SWI_WAKE_TOKEN',['../a00178.html#ga8f12e3b749e7b3dc905114cc7b8d0b38',1,'hal_swi_uart.h']]]
];
