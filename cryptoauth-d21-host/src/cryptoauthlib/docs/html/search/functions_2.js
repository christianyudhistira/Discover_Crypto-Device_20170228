var searchData=
[
  ['certdata_5funit_5ftests',['certdata_unit_tests',['../a00087.html#a3619f93a05811a3261c0b935acaef6b5',1,'certdata_unit_tests(void):&#160;atca_unit_tests.c'],['../a00088.html#a3619f93a05811a3261c0b935acaef6b5',1,'certdata_unit_tests(void):&#160;atca_unit_tests.c']]],
  ['certio_5funit_5ftests',['certio_unit_tests',['../a00087.html#aa3c04a6df37fbc3fa8dbd3b218a16557',1,'certio_unit_tests(void):&#160;atca_unit_tests.c'],['../a00088.html#aa3c04a6df37fbc3fa8dbd3b218a16557',1,'certio_unit_tests(void):&#160;atca_unit_tests.c']]],
  ['change_5fi2c_5fspeed',['change_i2c_speed',['../a00178.html#gab7f613b378e0d01b80703df3ac662d6d',1,'hal_sam4s_i2c_asf.c']]],
  ['cl_5fhash',['CL_hash',['../a00138.html#a955169bbbce9b4712cafb9b7372b5ffa',1,'CL_hash(U8 *msg, int msgBytes, U8 *dest):&#160;sha1_routines.c'],['../a00139.html#a955169bbbce9b4712cafb9b7372b5ffa',1,'CL_hash(U8 *msg, int msgBytes, U8 *dest):&#160;sha1_routines.c']]],
  ['cl_5fhashfinal',['CL_hashFinal',['../a00138.html#ab0ce5fa530aadfeaff8f411f129f7a2d',1,'CL_hashFinal(CL_HashContext *_ctx, U8 *dest):&#160;sha1_routines.c'],['../a00139.html#a672902e6406b4e4e2af33c70ec7a001c',1,'CL_hashFinal(CL_HashContext *ctx, U8 *dest):&#160;sha1_routines.c']]],
  ['cl_5fhashinit',['CL_hashInit',['../a00138.html#a7b91feb39f27d342257c26f17f565315',1,'CL_hashInit(CL_HashContext *_ctx):&#160;sha1_routines.c'],['../a00139.html#affa9482d686cc435d640d67d7fa79772',1,'CL_hashInit(CL_HashContext *ctx):&#160;sha1_routines.c']]],
  ['cl_5fhashupdate',['CL_hashUpdate',['../a00138.html#aefb93c278b545ef9d903701def53dd62',1,'CL_hashUpdate(CL_HashContext *_ctx, const U8 *src, int nbytes):&#160;sha1_routines.c'],['../a00139.html#a55a33e7a22aff4aca4bccf39897bc5f1',1,'CL_hashUpdate(CL_HashContext *ctx, const U8 *src, int nbytes):&#160;sha1_routines.c']]]
];
